#include <stdio.h>
#include<math.h>
#include<limits.h>
#include <stdlib.h>
int main()
{
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; i++)
    {
        int M1, M2, D;
        scanf("%d %d %d", &M1, &M2, &D);
        double R2 = D;
        double R1 = D*M1/(M1+M2);
        double reducedDays = R2-R1;

        printf("%.lf\n", reducedDays);
    }
    return 0;
}