#include <stdio.h>
#include <string.h>
int main()
{
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; i++)
    {
        char n[100001];
        scanf("%s", n);
        int s = 0, c = 0, d = 0;
        for (int i = 0; i < strlen(n); i++)
        {
            if (n[i] >= 'a' && n[i] <= 'z')
            {
                s++;
            }
            if (n[i] >= 'A' && n[i] <= 'Z')
            {
                c++;
            }
            if(n[i]>='0' && n[i]<='9')
            {
                d++;
            }
        }
        printf("%d %d %d",c,s,d);
        printf("\n");
    }
    return 0;
}