#include <stdio.h>
#include <string.h>

int main() {
    char S[10001];
    scanf("%s", S);
    int count[26] = {0}; 

    for (int i = 0; i < strlen(S); i++) {
        if (S[i] >= 97 && S[i] <= 122) { //ASCII Code: 'a-z'=97-122
            count[S[i] - 97]++;
        }
    }

    for (int i = 0; i < 26; i++) {
        if (count[i] > 0) {
            printf("%c - %d\n", 97 + i, count[i]);
        }
        // printf("%c - %d\n", 97 + i, count[i]);
    }

    return 0;
}
