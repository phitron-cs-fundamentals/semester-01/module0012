#include <stdio.h>
int main()
{
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; i++)
    {
        int n;
        scanf("%d", &n);
        int ar[n];
        for (int i = 0; i < n; i++)
        {
            scanf("%d ", &ar[i]);
        }
        int num;
        scanf("%d", &num);
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            if (ar[i] == num)
            {
                ans = 1;
                break;
            }
        }
        if (ans)
        {
            printf("YES\n");
        }
        else
        {
            printf("NO\n");
        }
    }
    return 0;
}